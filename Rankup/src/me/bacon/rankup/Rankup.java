package me.bacon.rankup;

import net.milkbowl.vault.economy.Economy;
import net.milkbowl.vault.permission.Permission;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.RegisteredServiceProvider;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.scoreboard.DisplaySlot;
import org.bukkit.scoreboard.Objective;
import org.bukkit.scoreboard.Scoreboard;
import org.bukkit.scoreboard.ScoreboardManager;

public class Rankup
        extends JavaPlugin
{
    public static Permission permission = null;
    public static Economy economy = null;
    public static Rankup plugin;
    public Listeners ev = new Listeners(this);


    public void onEnable()
    {
        plugin = this;
        loadConfiguration();
        setupPermissions();
        setupEconomy();
        getCommand("rankup").setExecutor(new RankupCommand(this));
        PluginManager pm = Bukkit.getPluginManager();
        pm.registerEvents(ev, this);

        Bukkit.getServer().getScheduler().scheduleSyncRepeatingTask(plugin, new Runnable(){
            public void run() {
                for (Player p : Bukkit.getServer().getOnlinePlayers()) {
                    ScoreboardHandler.scoreboard(p);
                }
            }
        }, 20l, 300l);
    }

    public void loadConfiguration()
    {
        getConfig().options().copyDefaults(true);
        saveConfig();
    }

    private boolean setupPermissions()
    {
        RegisteredServiceProvider<Permission> permissionProvider = getServer().getServicesManager().getRegistration(Permission.class);
        if (permissionProvider != null) {
            permission = (Permission)permissionProvider.getProvider();
        }
        return permission != null;
    }

    private boolean setupEconomy() {
        RegisteredServiceProvider<Economy> economyProvider = getServer().getServicesManager().getRegistration(Economy.class);
        if (economyProvider != null) {
            economy = (Economy) economyProvider.getProvider();
        }
        return economy != null;
    }
}
