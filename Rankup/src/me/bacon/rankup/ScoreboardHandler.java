package me.bacon.rankup;

import com.google.common.collect.Maps;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.scoreboard.*;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.UUID;

public class ScoreboardHandler {
    // the flishy flashy that is currently happening (or redo shit so it doesn't idk yet)
    // fix buggy shit in general
    static HashMap<UUID, ArrayList<String>> oldInfo = Maps.newHashMap();



    public static void scoreboard(Player player){
        ArrayList<String> info = oldInfo.get(player.getUniqueId());
        String rank = Rankup.permission.getPrimaryGroup(player);
        double price = Rankup.plugin.getConfig().getDouble("Price." + Rankup.plugin.getConfig().getString(new StringBuilder("Ranks.").append(rank).toString()));
        double balance = (int) Rankup.economy.getBalance(player.getName());
        double finalBalance = (int)price - balance;

        Scoreboard board = player.getScoreboard();
        Objective objective = board.getObjective(DisplaySlot.SIDEBAR);

        Score score1 = objective.getScore(ChatColor.translateAlternateColorCodes('&', "&9" + Rankup.plugin.getConfig().getString("Scoreboard.Balance")));
        Score balance1 = objective.getScore(ChatColor.translateAlternateColorCodes('&', "&b" + balance));
        Score space1 = objective.getScore(" ");
        Score space2 = objective.getScore("");
        Score space3 = objective.getScore("    ");
        Score score = objective.getScore(ChatColor.translateAlternateColorCodes('&', "" + Rankup.plugin.getConfig().getString("Scoreboard.Needed")));
        Score needed1 = objective.getScore(ChatColor.translateAlternateColorCodes('&', "&b" + finalBalance));
        Score rank2 = objective.getScore(ChatColor.translateAlternateColorCodes('&', "" + Rankup.plugin.getConfig().getString("Scoreboard.Rank")));
        Score rank1 = objective.getScore(ChatColor.translateAlternateColorCodes('&', "&b" + rank));
        Score online = objective.getScore(ChatColor.translateAlternateColorCodes('&', "&9" + "" + Rankup.plugin.getConfig().getString("Scoreboard.Online")));
        Score online1 = objective.getScore(ChatColor.translateAlternateColorCodes('&', "&b" + Bukkit.getServer().getOnlinePlayers().length));
        Score canRank = objective.getScore(ChatColor.translateAlternateColorCodes('&', Rankup.plugin.getConfig().getString("Scoreboard.CanRankup")));
        Score needed2 = objective.getScore(ChatColor.translateAlternateColorCodes('&', "&b" + "You can rankup"));

        if(info != null){
            //String score1 = info.get(0);
            for( String s : info.toArray(new String[0])){
                board.resetScores(s);
            }
        }
        info = new ArrayList<String>();

        if(balance < price) {
            score.setScore(8);
            needed1.setScore(7);;
        } else if( balance >= price){
            canRank.setScore(8);
            needed2.setScore(7);
        }
        score1.setScore(11);
        balance1.setScore(10);
        space2.setScore(9);
        space1.setScore(6);
        rank2.setScore(5);
        rank1.setScore(4);
        space3.setScore(3);
        online.setScore(2);
        online1.setScore(1);

        player.setScoreboard(board);

        info.add(balance1.getEntry());
        info.add(rank1.getEntry());
        info.add(online1.getEntry());
        info.add(score.getEntry());
        info.add(needed1.getEntry());
        info.add(needed2.getEntry());
        info.add(canRank.getEntry());

        oldInfo.put(player.getUniqueId(), info);
    }

    public static void setupBoard(Player player){
        ScoreboardManager manager = Bukkit.getServer().getScoreboardManager();
        Scoreboard board = manager.getNewScoreboard();
        Objective objective = board.registerNewObjective("Currency", "Dummy");
        objective.setDisplaySlot(DisplaySlot.SIDEBAR);
        objective.setDisplayName(ChatColor.translateAlternateColorCodes('&', "" + Rankup.plugin.getConfig().getString("ScoreboardName")));
        player.setScoreboard(board);
    }
}
