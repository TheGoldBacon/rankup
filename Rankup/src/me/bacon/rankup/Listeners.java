package me.bacon.rankup;

import org.bukkit.*;
import org.bukkit.entity.Firework;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.inventory.meta.FireworkMeta;

public class Listeners implements Listener {

    Rankup plugin;
    public Listeners(Rankup instance) {
        plugin = instance;
    }

    @EventHandler
    public void onPlayerClick(InventoryClickEvent e){
        Player p = (Player) e.getWhoClicked();
        String rank = Rankup.permission.getPrimaryGroup(p);
        String nextrank = this.plugin.getConfig().getString("Ranks." + rank.toString());
        double money = Rankup.economy.getBalance(p.getName());
        double price = this.plugin.getConfig().getDouble("Price." + this.plugin.getConfig().getString(new StringBuilder("Ranks.").append(rank.toString()).toString()));
        String broadcast = this.plugin.getConfig().getString("Messages.Broadcast").replaceAll("(&([a-f0-9]))", "§$2").replace("%player%", p.getName()).replace("%rank%", nextrank);
        if (!ChatColor.stripColor(e.getInventory().getName()).equalsIgnoreCase("Proceed With Rankup?"))
            return;
        e.setCancelled(true);

        if(e.getCurrentItem() == null || e.getCurrentItem().getType() == Material.AIR || !e.getCurrentItem().hasItemMeta()){
            p.closeInventory();
            return;
        }
        switch(e.getCurrentItem().getData().getData()){
            case 5:
                Rankup.economy.withdrawPlayer(p.getName(), price);
                Rankup.permission.playerRemoveGroup(p, rank);
                Rankup.permission.playerAddGroup(p, nextrank);
                this.plugin.getServer().broadcastMessage(broadcast);
                Firework f = (Firework) p.getWorld().spawn(p.getLocation(), Firework.class);
                FireworkMeta fm = f.getFireworkMeta();
                FireworkMeta fm1 = f.getFireworkMeta();
                fm.addEffect(FireworkEffect.builder().flicker(true).trail(true).withColor(Color.RED).withFade(Color.GREEN).build());
                fm1.addEffect(FireworkEffect.builder().flicker(true).trail(true).with(FireworkEffect.Type.BALL_LARGE).withColor(Color.ORANGE).withColor(Color.YELLOW).withColor(Color.PURPLE).withFade(Color.LIME).build());
                fm.setPower(1);
                fm1.setPower(1);
                f.setFireworkMeta(fm);
                f.setFireworkMeta(fm1);
                p.closeInventory();
                break;
            case 14:
                p.sendMessage(ChatColor.RED + "Sorry you did not rank up!");
                p.closeInventory();
                break;
            default:
                p.closeInventory();
                break;
        }
    }
   @EventHandler
    public void onJoin(final PlayerJoinEvent e){
        Player p = e.getPlayer();
       ScoreboardHandler.setupBoard(p);
    }
}
