package me.bacon.rankup;

import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.event.Listener;
import me.bacon.rankup.GUI;

public class RankupCommand
        implements CommandExecutor, Listener
{
    Rankup plugin;

    public RankupCommand(Rankup instance)
    {
        this.plugin = instance;
    }

    public boolean onCommand(CommandSender sender, Command cmd, String commandLabel, String[] args)
    {
        if (commandLabel.equalsIgnoreCase("rankup")) {
            if ((sender instanceof Player)) {
                Player p = (Player) sender;
                if (p.hasPermission("rankup.rankup")) {
                    String rank = Rankup.permission.getPrimaryGroup(p);
                    String nextrank = this.plugin.getConfig().getString("Ranks." + rank.toString());
                    if (!rank.equals(this.plugin.getConfig().getString("HighestRank"))) {

                        double money = Rankup.economy.getBalance(p.getName());
                        double price = this.plugin.getConfig().getDouble("Price." + this.plugin.getConfig().getString(new StringBuilder("Ranks.").append(rank.toString()).toString()));
                        String nomoney = this.plugin.getConfig().getString("Messages.NotEnoughMoney").replaceAll("(&([a-f0-9]))", "§$2");
                        String broadcast = this.plugin.getConfig().getString("Messages.Broadcast").replaceAll("(&([a-f0-9]))", "§$2").replace("%player%", p.getName()).replace("%rank%", nextrank);

                        if (money >= price) {
                            GUI.openGUI(p);
                        } else {
                            p.sendMessage(nomoney);
                        }
                    } else {
                        String highestrank = this.plugin.getConfig().getString("Messages.HighestRank").replaceAll("(&([a-f0-9]))", "Â§$2");
                        sender.sendMessage(highestrank);
                    }
                } else {
                    p.sendMessage(ChatColor.RED + "You dont have permission!");
                }
            } else {
                sender.sendMessage(ChatColor.RED + "Error: You must be a player to run that command!");
            }
        }
        return true;
    }
}