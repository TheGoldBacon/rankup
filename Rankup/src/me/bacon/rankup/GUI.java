package me.bacon.rankup;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.DyeColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.ArrayList;

public class GUI {
    public static void openGUI(Player player) {
        ArrayList<String> yl = new ArrayList<String>();
        ArrayList<String> nl = new ArrayList<String>();

        Inventory inv = Bukkit.createInventory(null, 9, ChatColor.translateAlternateColorCodes('&', "&c&l&oProceed with Rankup?"));

        ItemStack yes = new ItemStack(Material.STAINED_GLASS_PANE, 1, DyeColor.LIME.getData());
        ItemStack no = new ItemStack(Material.STAINED_GLASS_PANE, 1, DyeColor.RED.getData());

        ItemMeta ym = yes.getItemMeta();
        ItemMeta nm = no.getItemMeta();

        yl.add(ChatColor.AQUA + "Clicking this will rank you up to the next rank!");
        nl.add(ChatColor.AQUA + "Clicking this will exit out of the menu, you will not rankup!");

        nm.setLore(nl);
        ym.setLore(yl);

        ym.setDisplayName(ChatColor.translateAlternateColorCodes('&', "&a&lAccept Rankup"));
        nm.setDisplayName(ChatColor.translateAlternateColorCodes('&', "&c&lDeny Rankup"));

        yes.setItemMeta(ym);
        no.setItemMeta(nm);

        inv.setItem(0, yes);
        inv.setItem(8, no);

        player.openInventory(inv);
    }
}
